import axios from 'axios';
import { REGISTER } from './serviceEndPoints';

const userService = {

    async save (data) {
        await axios.post(REGISTER, data)
    }

}

export default userService
