import React, { useState } from 'react';
import { View, Text, TouchableOpacity, TextInput} from 'react-native';
import UserService from '../../service/UserService';

import styles from './styles';

export default function RegisterUser({ navigation }) {
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [isValid, setIsValid] = useState(true);
    const [message, setMessage] = useState('');

    const validateRegister = () => {
        //temporário - usar framework para validação
        if ( email == '' || email == null) {
            setMessage('Email inválido')
            setIsValid(false);
        } else if (phone == '' || phone == null) {
            setMessage('Phone inválido')
            setIsValid(false);
        } else if (password == '' || password == null) {
            setMessage('Password inválido')
            setIsValid(false);
        }
        if (isValid) { 
            save()
        }
    }

    const save = async () => {
        await UserService.save({
            email: email,
            phone: phone,
            password: password
        })

        navigation.goBack()
    }

    return (
        <View style={styles.container} >
            <Text style={styles.primaryText}>Cadastro de usuário</Text>
            
            <TextInput keyboardType='email-address' placeholder='Email' placeholderTextColor='#000' style={styles.inputText} 
                  onChangeText={value => setEmail(value)} value={email} />
            <TextInput keyboardType='phone-pad' placeholder='Telefone' placeholderTextColor='#000' style={styles.inputText} 
                  onChangeText={value => setPhone(value)} value={phone} />
            <TextInput secureTextEntry placeholder='Senha'  placeholderTextColor='#000' style={styles.inputText} 
                  onChangeText={value => setPassword(value)} value={password} />

            <Text style={styles.message}>{message}</Text>
            

            <TouchableOpacity 
                onPress={ () => validateRegister}
                style={styles.button} 
            >
               <Text style={styles.text} > Cadastrar </Text>
            </TouchableOpacity>
        </View>
    )
}