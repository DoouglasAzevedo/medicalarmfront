import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems :'center',
        justifyContent: 'center',
    },

    inputText: {
        width: '80%',
        marginTop: 20,
        borderBottomWidth : 1,
        borderBottomColor: '#4d4d4d',
        fontSize: 16,
        color: '#000'
    },

    button: {
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: '#b31aff',
        height: 40,
        borderRadius: 10
    },

    text: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'white'
    },

});

export default styles;