import React from 'react';
import { View, Text, TouchableOpacity, Image, TextInput} from 'react-native';

import styles from './styles';

export default function Login({ navigation }) {
    return (
        <View style={styles.container} >
            <Image source={require('../../assets/Logo.png')} />

            <TextInput keyboardType='email-address' placeholder='Email' placeholderTextColor='#000' style={styles.inputText}/>
            <TextInput secureTextEntry placeholder='Senha' placeholderTextColor='#000' style={styles.inputText}/>
            
            <TouchableOpacity 
                onPress={() => navigation.navigate('MedicineList')}
                style={styles.button} >
                <Text style={styles.text} > Entrar </Text>
            </TouchableOpacity>

            <TouchableOpacity 
                onPress={() => navigation.navigate('RegisterUser')} 
                style={styles.button} 
            >
                <Text style={styles.text} > Cadastre-se </Text>
            </TouchableOpacity>

        </View>
    )
}

