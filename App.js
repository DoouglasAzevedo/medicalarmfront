import React from 'react';
import Login from './src/pages/Login';
import RegisterUser from './src/pages/Register';
import MedicineList from './src/pages/MedicineList';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login" >
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="RegisterUser" component={RegisterUser} options={{ headerShown: false }} />
        <Stack.Screen name="MedicineList" component={MedicineList} options={{ headerShown: false }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
